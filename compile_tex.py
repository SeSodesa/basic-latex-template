## compile_tex.py
#
# Copyright © Santtu Söderholm 2023
#
# An opinionated Python program for compiling LaTeΧ projects. First compiles
# any TikZ figures that might be located inside an `images/` folder and then
# the main file. Call with optional timeout as follows.
#
#     python3 compile_tex.py project_folder [timeout] [texcompiler] [bibcompiler]
#
# Any value convertible to a positive number will be interpreted as a
# [timeout], [texcompiler] will be set as the last instance of pdflatex or
# lualatex, and [bibcompiler] will be the last instance of bibtex or biber.
#
# NOTE: every file that is supposed to be compiled by this script needs to be
# named "main.tex". Put the files in the figures folder in their own
# subfolders.

## Imports

import os
import re
import sys
import subprocess
from pathlib import Path
from shutil import which

## Constants

STANDALONE_AUTOMATON = re.compile(r"\\documentclass.*\{standalone\}")
TIKZ_BEGIN_AUTOMATON = re.compile(r"\\begin\{tikzpicture\}")
TIKZ_END_AUTOMATON = re.compile(r"\\end\{tikzpicture\}")

## Functions

"""
Checks that the given TeΧ file is a valid enough image source for the program
to even dare tying to call a LaTeΧ compiler on it. It must be a standalone
image, and contain a TikZpicture environment.
"""
def is_likely_valid_image_source(image_source_file: Path):

	with open(image_source_file) as isf:

		# Read file contents

		ist = isf.read()

		# Check for standalone image and existence of a TikZ environment.

		is_standalone = STANDALONE_AUTOMATON.search(ist) is not None
		has_tikz_start = TIKZ_BEGIN_AUTOMATON.search(ist) is not None
		has_tikz_end = TIKZ_END_AUTOMATON.search(ist) is not None

		is_likely_valid = (
			is_standalone
			and
			has_tikz_start
			and
			has_tikz_end
		)

		return is_likely_valid

"""
Compiles a single given image, assuming it exists and has the required
contents.
"""
def compile_image(image_source: Path):

	if not is_likely_valid_image_source(image_source):
		return

	image_folder = image_source.parent

	# Yeah, we are using PDFLaTeΧ for compiling the images.

	texpath = which("lualatex")

	if not texpath:
		print("LuaLaTeΧ not found. Aborting…".format(texpath))
		return

	texargs = [
		texpath,
		"--halt-on-error",
		image_source
	]

	if image_folder.is_dir():
		os.chdir(image_folder)
	else:
		print("Could not change working directory to {}".format(image_folder))
		return

	print("Compiling image {0}".format(image_folder))

	with open(os.devnull, 'w') as fp:
		exitcode = subprocess.call(texargs, stdout=fp)


"""
Either recursively calls itself to navigate to a subfolder or calls `lualatex`
on an image in the current folder.
"""
def compile_images_in(image_folder: Path):

	image_source = image_folder / "main.tex"

	for child in image_folder.iterdir():

		if child.is_dir():
			compile_images_in(child)

		do_compile_file = (
			image_source.is_file()
			and
			child == image_source
		)

		if do_compile_file:
			compile_image(child)


"""
The main function of this program. Gets the project folder as input from
`stdin`.
"""
def compile_tex_main(arg_project_folder, arg_timeout, texcompiler, bibcompiler):

	# Check that system has LaTeΧ installation.

	texpath = which(texcompiler)

	biberpath = which(bibcompiler)

	if not texpath:
		print("PDFLaTeΧ not found on system path. Aborting…".format(texpath))
		return

	if not biberpath:
		print("Biber not found on system path. Aborting…".format(texpath))
		return

	# Check that given input folder was valid.

	try:
		project_path = Path(arg_project_folder).resolve(strict=True)
	except FileNotFoundError:
		error_msg = "Given project directory {0} is not valid".format(
			str ( arg_project_folder )
		)

		print(error_msg)

		return -1

	if not project_path.is_dir():

		error_msg = "Given project \"directory\" {0} is not a directory".format(
			str ( arg_project_path )
		)

		print(error_msg)

		return -2

	mainfile = project_path / "main.tex"

	if not mainfile.is_file():
		print("Main project \"file\" is not a file. Aborting…")
		return

	# Folder was valid enough, so switch to it.

	os.chdir(project_path)

	# Check whether images exist and compile them.

	figure_path = project_path / "images"

	if figure_path.is_dir():
		compile_images_in(figure_path)

	# Make sure we are at project root after image compilation and compile the
	# main LaTeΧ file, if possible.

	os.chdir(project_path)

	print("Compiling main file {0}".format(mainfile))

	texargs = [ texpath, "--halt-on-error", mainfile ]

	bibargs = [biberpath, "main"]

	with open(os.devnull, 'w') as fp:

		print("First run of " + texcompiler + " (timeout in {} s)…".format(arg_timeout))

		try:
			exitcode = subprocess.call(texargs, stdout=fp, timeout=arg_timeout)
		except TimeoutExpired as e:
			print("Timeout expired. Aborting…")
			return

		if exitcode:
			print("Could not run " + texcompiler + " on {}. Aborting…".format(mainfile))
			return

		print("Run " + bibcompiler + " (timeout in {} s)…".format(arg_timeout))

		try:
			exitcode = subprocess.call(bibargs, stdout=fp, timeout=arg_timeout)
		except TimeoutExpired as e:
			print("Timeout expired. Aborting…")
			return

		if exitcode:
			print("Could not run " + bibcompiler + " on {}. Aborting…".format(mainfile))
			return

		print("Second run of " + texcompiler + " (timeout in {} s)…".format(arg_timeout))

		try:
			exitcode = subprocess.call(texargs, stdout=fp, timeout=arg_timeout)
		except TimeoutExpired as e:
			print("Timeout expired. Aborting…")
			return

		if exitcode:
			print("Could not run " + texcompiler + " on {} a 2nd time. Aborting…".format(mainfile))
			return

		print("Third run of " + texcompiler + " (timeout in {} s)…".format(arg_timeout))

		try:
			exitcode = subprocess.call(texargs, stdout=fp, timeout=arg_timeout)
		except TimeoutExpired as e:
			print("Timeout expired. Aborting…")
			return

		if exitcode:
			print("Could not run " + texcompiler + " on {} a 3rd time. Aborting…".format(mainfile))
			return

	print("Done.")


# Do not call the main function unless this file is the main file.

if __name__ == "__main__":

	project_folder = Path ( sys.argv[1] )

	try:

		project_folder = project_folder.resolve(strict=True)

	except FileNotFoundError:

		print ( sys.argv[0] + ": Could not resolve the given TeΧ project path " + str ( project_folder ) )

	# Default settings.

	timeout = 10

	texcompiler = "pdflatex"

	bibcompiler = "bibtex"

	# Check if default settings should be overwritten.

	for arg in sys.argv[2:]:

		if arg in ["pdflatex", "lualatex"]:

			texcompiler = arg

		if arg in ["bibtex", "biber"]:

			bibcompiler = arg

		try:

			timeout_candidate = float ( arg )

		except ValueError:

			timeout_candidate = timeout

		if timeout_candidate > 0:

			timeout = timeout_candidate

	# Run main function.

	compile_tex_main(project_folder, timeout, texcompiler, bibcompiler)
