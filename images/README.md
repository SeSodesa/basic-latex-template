# images/

Place any images here. They can be referenced from the text files by a path
relative to the [`main.tex`](../main.tex) file, such as
`images/some-image.pdf`.
