# Basic LaTeX Template

With this template, one should simply be able to start writing a simple [LaTeΧ]
document by adding text to the files in the folder [`src/`](./src). The
document can be compiled with a LaTeΧ compiler `lc` ∈ {`pdflatex`,`lualatex`}
by typing
```sh
lc main.tex && biber main && main.tex && main.tex
```
in a shell emulator of their choice. Alternatively, one might download this
repository as a ZIP file and upload it to an online LaTeΧ service such as
[Overleaf] by going to their [Overleaf projects page] and pressing <kbd>New
project</kbd> → <kbd>Upload project</kbd> and choosng the downloaded ZIP file
from the file browser.

[LaTeΧ]: https://www.latex-project.org/about/
[Overleaf]: https://en.wikipedia.org/wiki/Overleaf
[Overleaf projects page]: https://www.overleaf.com/project

## License

Copyright © Santtu Söderholm 2023. See the [`LICENSE`](./LICENSE) file for the
licensing information.
